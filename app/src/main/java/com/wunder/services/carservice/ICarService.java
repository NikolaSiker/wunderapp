package com.wunder.services.carservice;

import com.wunder.model.Car;
import com.wunder.model.CarDetails;
import com.wunder.services.IUrlConstants;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;

import static com.wunder.common.Constants.CAR_ID;

/**
 * Interface containing all methods for making rest requests for Cars
 */
public interface ICarService {

    @GET(IUrlConstants.GET_ALL_CARS_ENDPOINT)
    Single<Response<List<Car>>> getAllCars();

    @GET(IUrlConstants.GET_CAR_BY_ID_ENDPOINT)
    Single<Response<CarDetails>> getCarById(@Path(CAR_ID) String id);
}
