package com.wunder.services;


//Interface that contains URL constants for rest api

public interface IUrlConstants {

    //region CAR ENDPOINTS
    public static String BASE_URL = "https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/";
    public static String GET_ALL_CARS_ENDPOINT = "cars.json";
    public static String GET_CAR_BY_ID_ENDPOINT = "cars/{carId}";
    //endregion


    //region RENT ENDPOINTS
    public static String POST_RENT_CAR_ENDPOINT = "https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-mobile-dev-quick-rental";
    //endregion

    public static String AUTHORIZATION_TOKEN = "Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa";
}
