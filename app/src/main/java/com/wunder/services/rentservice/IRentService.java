package com.wunder.services.rentservice;

import io.reactivex.Single;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Interface containing all methods for making rest requests for Rents
 */
public interface IRentService {
    @POST
    Single<ResponseBody> createRent(@Url String url, @Header("Authorization") String token, @Body RequestBody rentObject);
}
