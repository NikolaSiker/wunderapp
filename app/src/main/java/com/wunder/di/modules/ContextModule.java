package com.wunder.di.modules;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * ContextModule is class used to provide application object and base context
 */
@Module
public class ContextModule {

    private Application application;

    /**
     * Class constructor
     *
     * @param application - Application reference
     */
    public ContextModule(Application application) {
        this.application = application;
    }


    /**
     * Method for providing application object to Injects
     *
     * @return application object
     */
    @Provides
    @Singleton
    Application provideApplication() {
        return this.application;
    }


    /**
     * Method for providing base context object to Inject
     *
     * @return base context object
     */
    @Provides
    @Singleton
    Context provideContext() {
        return this.application.getBaseContext();
    }
}
