package com.wunder.di.modules;

import com.wunder.services.IUrlConstants;
import com.wunder.services.carservice.ICarService;
import com.wunder.services.rentservice.IRentService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class which provides network objects
 */

@Module
public class NetworkModule {

    /**
     * Method that provides retrofit instance for API calling
     *
     * @return retrofit instance
     */
    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit
                .Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(IUrlConstants.BASE_URL)
                .build();
    }

    /**
     * Method that builds and returns service instance
     *
     * @param retrofit for building service
     * @return service instance
     */
    @Provides
    @Singleton
    ICarService provideCarService(Retrofit retrofit) {
        return retrofit.create(ICarService.class);
    }

    @Provides
    @Singleton
    IRentService provideRentService(Retrofit retrofit) {
        return retrofit.create(IRentService.class);
    }
}
