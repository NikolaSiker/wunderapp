package com.wunder.di.modules;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.wunder.repositories.CarRepository;
import com.wunder.repositories.RentRepository;
import com.wunder.vm.CarDetailsViewModel;
import com.wunder.vm.MapsViewModel;
import com.wunder.vm.ViewModelFactory;
import com.wunder.vm.ViewModelKey;

import java.util.Map;

import javax.inject.Provider;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;


/**
 * Class that provides ViewModels
 */
@Module
public class ViewModelModule {

    /**
     * Provides MapsViewModel instance to Injects
     *
     * @param carRepository - REST repository for calling service methods
     * @return MapsViewModel instance
     */
    @Provides
    @IntoMap
    @ViewModelKey(MapsViewModel.class)
    ViewModel bindMapsViewModel(CarRepository carRepository) {
        return new MapsViewModel(carRepository);
    }

    /**
     * Provides CarDetailsViewModel instance to Injects
     *
     * @param carRepository - REST repository for calling service methods
     * @return CarDetails instance
     */
    @Provides
    @IntoMap
    @ViewModelKey(CarDetailsViewModel.class)
    ViewModel bindCarDetailsViewModel(CarRepository carRepository, RentRepository rentRepository) {
        return new CarDetailsViewModel(carRepository, rentRepository);
    }

    /**
     * Provides ViewModelFactory instance to Injects
     *
     * @param providerMap - provider map with ViewModel values
     * @return ViewModelFactory instance
     */
    @Provides
    ViewModelProvider.Factory bindViewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> providerMap) {
        return new ViewModelFactory(providerMap);
    }
}
