package com.wunder.di.components;

import com.wunder.MainActivity;
import com.wunder.di.modules.ContextModule;
import com.wunder.di.modules.NetworkModule;
import com.wunder.di.modules.ViewModelModule;
import com.wunder.views.detailsview.DetailsActivity;
import com.wunder.views.mapview.MapActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * ApplicationComponent interface is used to link all modules and declare where injections can be done
 */
@Singleton
@Component(modules = {ContextModule.class, NetworkModule.class, ViewModelModule.class})
public interface ApplicationComponent {
    void inject(MapActivity mapActivity);

    void inject(DetailsActivity detailsActivity);

    void inject(MainActivity mainActivity);
}
