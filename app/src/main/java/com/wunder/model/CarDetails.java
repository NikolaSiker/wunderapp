package com.wunder.model;

public class CarDetails extends Car {
    private String vehicleTypeImageUrl;
    private String damageDescription;
    private String hardwareId;
    private Boolean isActivatedByHardware;

    public String getVehicleTypeImageUrl() {
        return vehicleTypeImageUrl;
    }

    public void setVehicleTypeImageUrl(String vehicleTypeImageUrl) {
        this.vehicleTypeImageUrl = vehicleTypeImageUrl;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public void setDamageDescription(String damageDescription) {
        this.damageDescription = damageDescription;
    }

    public String getHardwareId() {
        return hardwareId;
    }

    public void setHardwareId(String hardwareId) {
        this.hardwareId = hardwareId;
    }

    public Boolean getActivatedByHardware() {
        return isActivatedByHardware;
    }

    public void setActivatedByHardware(Boolean activatedByHardware) {
        isActivatedByHardware = activatedByHardware;
    }
}
