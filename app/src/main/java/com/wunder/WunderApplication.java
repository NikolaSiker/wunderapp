package com.wunder;

import android.app.Application;

import com.wunder.di.components.ApplicationComponent;
import com.wunder.di.components.DaggerApplicationComponent;
import com.wunder.di.modules.ContextModule;
import com.wunder.di.modules.NetworkModule;
import com.wunder.di.modules.ViewModelModule;

public class WunderApplication extends Application {

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent
                .builder()
                .networkModule(new NetworkModule())
                .contextModule(new ContextModule(this))
                .viewModelModule(new ViewModelModule())
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
