package com.wunder.vm;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.wunder.model.Car;
import com.wunder.repositories.CarRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MapsViewModel extends ViewModel {

    private static final String TAG = "MapsVM";
    private CarRepository carRepository;
    private CompositeDisposable disposable;

    private final MutableLiveData<List<Car>> cars = new MutableLiveData<>();

    private final MutableLiveData<Boolean> carsLoadError = new MutableLiveData<>();

    private Car selectedCar = null;

    /**
     * Class constructor
     *
     * @param carRepository
     */
    @Inject
    public MapsViewModel(CarRepository carRepository) {
        this.carRepository = carRepository;
        disposable = new CompositeDisposable();
    }

    public MutableLiveData<List<Car>> getCars() {
        return cars;
    }

    public MutableLiveData<Boolean> getCarsLoadError() {
        return carsLoadError;
    }

    public Car getSelectedCar() {
        return selectedCar;
    }

    public void setSelectedCar(Car car) {
        this.selectedCar = car;
    }

    /**
     * Method for fetching all cars from server
     */
    public void fetchCars() {
        Log.d(TAG, "Fetching cars...");
        disposable.add(carRepository
                .getAllCars()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listResponse -> {
                    if (listResponse.code() == 200) {
                        cars.setValue(listResponse.body());
                        carsLoadError.setValue(false);
                        Log.d(TAG, "Cars fetched; code = " + listResponse.code());
                    } else {
                        carsLoadError.setValue(true);
                        Log.d(TAG, "Cars failed to fetch; code = " + listResponse.code());
                    }
                }, throwable -> {
                    carsLoadError.setValue(true);
                    Log.d(TAG, "Cars fetched; message = " + throwable.getLocalizedMessage());
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();

        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}
