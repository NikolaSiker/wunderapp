package com.wunder.vm;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.wunder.model.CarDetails;
import com.wunder.model.RentResponseModel;
import com.wunder.repositories.CarRepository;
import com.wunder.repositories.RentRepository;
import com.wunder.services.IUrlConstants;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;

public class CarDetailsViewModel extends ViewModel {

    private static final String TAG = "CarDetailsVM";


    private CarRepository carRepository;
    private RentRepository rentRepository;

    private CompositeDisposable carDisposable;
    private CompositeDisposable rentDisposable;

    private MutableLiveData<CarDetails> carData = new MutableLiveData<>();

    private MutableLiveData<Boolean> loadingCar = new MutableLiveData<>();

    private MutableLiveData<Boolean> errorCarData = new MutableLiveData<>();

    private MutableLiveData<RentResponseModel> rentResponse = new MutableLiveData<>();

    private MutableLiveData<Boolean> rentError = new MutableLiveData<>();

    private MutableLiveData<Boolean> rentLoading = new MutableLiveData<>();

    /**
     * Class constructor
     *
     * @param carRepository
     * @param rentRepository
     */
    @Inject
    public CarDetailsViewModel(CarRepository carRepository, RentRepository rentRepository) {
        this.carRepository = carRepository;
        this.rentRepository = rentRepository;
        carDisposable = new CompositeDisposable();
        rentDisposable = new CompositeDisposable();
    }

    public MutableLiveData<CarDetails> getCarData() {
        return carData;
    }

    public MutableLiveData<Boolean> getLoadingCar() {
        return loadingCar;
    }

    public MutableLiveData<Boolean> getErrorCarData() {
        return errorCarData;
    }

    /**
     * Method for fetching car by id from server
     *
     * @param id
     */
    public void fetchCar(String id) {
        Log.d(TAG, "Fetching data...");
        loadingCar.setValue(true);
        carDisposable.add(carRepository
                .getCarByid(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(carResponse -> {
                    loadingCar.setValue(false);
                    if (carResponse.code() == 200) {
                        Log.d(TAG, "Data fetched; code =" + carResponse.code());
                        carData.setValue(carResponse.body());
                        errorCarData.setValue(false);
                    } else {
                        Log.d(TAG, "Data failed to fetch; code =" + carResponse.code());
                        errorCarData.setValue(true);
                    }
                }, throwable -> {
                    loadingCar.setValue(false);
                    errorCarData.setValue(true);
                    Log.d(TAG, "Data failed to fetch; message = " + throwable.getLocalizedMessage());
                }));
    }

    /**
     * Method for creating rent on server
     *
     * @param requestBody
     */
    public void createRent(RequestBody requestBody) {
        Log.d(TAG, "Creating rent...");
        rentLoading.setValue(true);
        rentDisposable.add(this.rentRepository.createRent(IUrlConstants.POST_RENT_CAR_ENDPOINT, IUrlConstants.AUTHORIZATION_TOKEN, requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> {
                    rentLoading.setValue(false);
                    if (responseBody != null) {
                        Log.d(TAG, "Rent successfuly created; ");
                        rentResponse.setValue(new Gson().fromJson(responseBody.string(), RentResponseModel.class));
                    }
                }, throwable -> {
                    rentLoading.setValue(false);
                    rentError.setValue(true);
                    Log.d(TAG, "Rent failed to create; message = " + throwable.getLocalizedMessage());
                }));
    }


    public MutableLiveData<RentResponseModel> getRentResponse() {
        return rentResponse;
    }

    public MutableLiveData<Boolean> getRentError() {
        return rentError;
    }

    public MutableLiveData<Boolean> getRentLoading() {
        return rentLoading;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (carDisposable != null) {
            carDisposable.clear();
            carDisposable = null;
        }

        if (rentDisposable != null) {
            rentDisposable.clear();
            rentDisposable = null;
        }
    }
}
