package com.wunder.views.detailsview;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wunder.R;
import com.wunder.WunderApplication;
import com.wunder.common.Utils;
import com.wunder.model.Car;
import com.wunder.model.CarDetails;
import com.wunder.model.RentModel;
import com.wunder.vm.CarDetailsViewModel;
import com.wunder.vm.ViewModelFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.wunder.common.Constants.CAR_ID;

public class DetailsActivity extends AppCompatActivity {

    private static final String TAG = "DetailsActivity";
    @Inject
    protected ViewModelFactory viewModelFactory;

    @BindView(R.id.activityDetails_image)
    protected ImageView activityDetailsImage;
    @BindView(R.id.activityDetails_table)
    protected TableLayout activityDetailsTable;
    @BindView(R.id.activityDetails_titleText)
    protected TextView activityDetailsTitleText;
    @BindView(R.id.activityDetails_addressText)
    protected TextView activityDetailsAddressText;
    @BindView(R.id.activityDetails_rentButton)
    protected Button activityDetailsRentButton;
    @BindView(R.id.activityDetails_dataWrapper)
    protected RelativeLayout activityDetailsDataWrapper;
    @BindView(R.id.activityDetails_loadingWrapper)
    protected LinearLayout activityDetailsLoadingWrapper;
    @BindView(R.id.errorLayout_retryButton)
    protected Button activityDetailsRetryButton;
    @BindView(R.id.activityDetails_errorWrapper)
    protected LinearLayout activityDetailsErrorWrapper;
    @BindView(R.id.activityDetails_rentProgressBar)
    protected ProgressBar activityDetailsRentProgressBar;
    @BindView(R.id.activityDetails_noConnectionWrapper)
    protected LinearLayout activityDetailsNoConnectionWrapper;

    private CarDetailsViewModel carDetailsViewModel;

    private Integer carId;

    private boolean isDataSet;

    private JSONObject fieldsObject = new JSONObject();

    private boolean isConnectedToInternet;

    private CompositeDisposable connectionDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        Log.d(TAG, "DetailsActivity created...");
        initUi();
    }

    /**
     * Method for UI initialization
     */
    private void initUi() {
        Log.d(TAG, "Initing UI...");
        initData();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        carDetailsViewModel.getLoadingCar().observe(this, loading -> {
            if (loading) {
                showLoadingLayout();
                Log.d(TAG, "Car data loading...");
            } else {
                showDataLayout();
                Log.d(TAG, "Car data loaded");
            }
        });

        carDetailsViewModel.getErrorCarData().observe(this, error -> {
            if (error) {
                if (!isConnectedToInternet) {
                    showNoConnection();
                    Log.d(TAG, "Connection error");
                } else {
                    showErrorLayout();
                    Log.d(TAG, "Error encountered");
                }
            } else {
                showDataLayout();
                Log.d(TAG, "No errors");
            }
        });

        activityDetailsRetryButton.setOnClickListener(listener -> {
            fetchCar();
            Log.d(TAG, "Retry clicked...");
        });

        activityDetailsRentButton.setOnClickListener(v -> {
            Log.d(TAG, "Rent clicked...");
            carDetailsViewModel.createRent(buildRequestBody());
        });


        carDetailsViewModel.getRentResponse().observe(this, response -> {
            if (response != null) {
                Log.d(TAG, "Rent successful; carId" + response.getCarId());
                showSnackBar(getResources().getString(R.string.successful_rent_text));
            }
        });

        carDetailsViewModel.getRentLoading().observe(this, loading -> {
            if (loading) {
                showRentLoading();
                Log.d(TAG, "Renting...");
            } else {
                Log.d(TAG, "Rented");
                showRentButton();
            }
        });

        carDetailsViewModel.getRentError().observe(this, error -> {
            if (error) {
                showSnackBar(getResources().getString(R.string.failed_rent_text));
                Log.d(TAG, "Rent failed; Check VM");
            }
        });


        carDetailsViewModel.getCarData().observe(this, carDetails -> {
            Log.d(TAG, "Car fetched; carId = " + carDetails.getCarId());
            setUiData(carDetails);
            createJSONObject(carDetails);
            setTableData();
        });
        Log.d(TAG, "UI inited");
    }

    /**
     * Method for data initialization including ViewModel
     */
    private void initData() {
        Log.d(TAG, "Initing data");
        ((WunderApplication) getApplication()).getApplicationComponent().inject(this);

        carDetailsViewModel = ViewModelProviders.of(this, viewModelFactory).get(CarDetailsViewModel.class);

        carId = getIntent().getExtras().getInt(CAR_ID);

        fetchCar();

        connectionDisposable.add(Utils.provideInternetConnectionInfo().subscribe(aBoolean -> isConnectedToInternet = aBoolean));

        Log.d(TAG, "Data inited");
    }

    /**
     * Method for calling fetch method from VM
     */
    private void fetchCar() {
        carDetailsViewModel.fetchCar(String.valueOf(carId));
    }

    /**
     * Method for showing no connection layout
     */
    private void showNoConnection() {
        activityDetailsErrorWrapper.setVisibility(View.GONE);
        activityDetailsDataWrapper.setVisibility(View.GONE);
        activityDetailsNoConnectionWrapper.setVisibility(View.VISIBLE);
    }


    /**
     * Method for showing rent loading state layout
     */
    private void showRentLoading() {
        activityDetailsRentButton.setVisibility(View.GONE);
        activityDetailsRentProgressBar.setVisibility(View.VISIBLE);
    }


    /**
     * Method for showing rent button
     */
    private void showRentButton() {
        activityDetailsRentProgressBar.setVisibility(View.GONE);
        activityDetailsRentButton.setVisibility(View.VISIBLE);
    }


    /**
     * Method for showing snackbar with feedback message
     */
    private void showSnackBar(String message) {
        Snackbar.make(activityDetailsTable, message, Snackbar.LENGTH_LONG).show();
    }


    /**
     * Method for showing data layout when loading is finished
     */
    private void showDataLayout() {
        activityDetailsLoadingWrapper.setVisibility(View.GONE);
        activityDetailsDataWrapper.setVisibility(View.VISIBLE);
        activityDetailsErrorWrapper.setVisibility(View.GONE);
    }


    /**
     * Method for showing car loading state
     */
    private void showLoadingLayout() {
        activityDetailsLoadingWrapper.setVisibility(View.VISIBLE);
        activityDetailsDataWrapper.setVisibility(View.GONE);
        activityDetailsErrorWrapper.setVisibility(View.GONE);
    }


    /**
     * Method for showing error state layout
     */
    private void showErrorLayout() {
        activityDetailsLoadingWrapper.setVisibility(View.GONE);
        activityDetailsDataWrapper.setVisibility(View.GONE);
        activityDetailsErrorWrapper.setVisibility(View.VISIBLE);
    }


    /**
     * Method for building request body for sending to REST API
     */
    private RequestBody buildRequestBody() {
        Log.d(TAG, "Building request body...");
        RentModel rentModel = new RentModel();
        rentModel.setCarId(carId);
        Log.d(TAG, "Request body built");
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), new Gson().toJson(rentModel));
    }


    /**
     * Method which converts car details object to JSON and extracts all properties and values and show them in table
     */
    private void setTableData() {
        Log.d(TAG, "Setting table data");
        if (!isDataSet) {
            Iterator<String> iter = fieldsObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    Object value = fieldsObject.get(key);

                    activityDetailsTable.addView(createTableRow(key, String.valueOf(value)));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.d(TAG, "Table data set");

            isDataSet = true;

        }
    }

    private void createJSONObject(CarDetails carDetails) {
        try {
            fieldsObject = new JSONObject(new Gson().toJson(carDetails));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Setting UI data based on provided car details object
     *
     * @param carDetails
     */
    private void setUiData(CarDetails carDetails) {
        Glide.with(this).load(carDetails.getVehicleTypeImageUrl()).into(activityDetailsImage);
        activityDetailsTitleText.setText(getCarTitle(carDetails));
        activityDetailsAddressText.setText(carDetails.getAddress());
    }

    /**
     * Method for creating table row view and setting values to a text views
     *
     * @param key
     * @param value
     * @return
     */
    private TableRow createTableRow(String key, String value) {
        TableRow row = new TableRow(this);

        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(lp);

        TextView nameText = new TextView(this);
        TextView valueText = new TextView(this);


        valueText.setLayoutParams(lp);
        nameText.setLayoutParams(lp);

        nameText.setText(key);
        valueText.setText(value);

        nameText.setSingleLine();
        valueText.setSingleLine();

        valueText.setEllipsize(TextUtils.TruncateAt.END);

        nameText.setBackgroundResource(R.drawable.table_cell_shape);
        valueText.setBackgroundResource(R.drawable.table_cell_shape);


        nameText.setPadding(10, 10, 10, 10);
        valueText.setPadding(10, 10, 10, 10);

        row.addView(nameText);
        row.addView(valueText);

        return row;
    }

    /**
     * Method for formatting car title depending on its existence
     *
     * @param car
     * @return
     */
    private String getCarTitle(Car car) {
        if (car.getTitle() == null || car.getTitle().trim().isEmpty()) {
            return "No title";
        }
        return car.getTitle();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (connectionDisposable != null) {
            connectionDisposable.clear();
            connectionDisposable = null;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
