package com.wunder.views.mapview;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.wunder.R;
import com.wunder.WunderApplication;
import com.wunder.common.Utils;
import com.wunder.model.Car;
import com.wunder.views.detailsview.DetailsActivity;
import com.wunder.vm.MapsViewModel;
import com.wunder.vm.ViewModelFactory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.wunder.common.Constants.CAR_ID;

public class MapActivity extends AppCompatActivity implements
        OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    private static final String TAG = "MapsActivity";
    @Inject
    protected ViewModelFactory viewModelFactory;

    private MapsViewModel mapsViewModel;

    private List<Marker> markerList = new ArrayList<>();

    private final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 100;

    private boolean mLocationPermissionGranted = false;

    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationClient;
    private GoogleMap googleMap;

    private boolean isCurrentLocationInited = false;

    private Snackbar snackbar;

    private CompositeDisposable connectionDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Log.d(TAG, "MapsActivity created...");
        initUi();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.d(TAG, "Map ready...");

        this.googleMap = googleMap;

        getCarsData(googleMap);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMapClickListener(this);

        if (mLocationPermissionGranted) {
            Log.d(TAG, "Setting user location...");

            googleMap.getUiSettings().setMyLocationButtonEnabled(true);

            buildLocationRequest();

            buildFusedLocationClient();
        }
    }

    /**
     * Method for building fused location client needed for getting users location
     */
    @SuppressLint("MissingPermission")
    private void buildFusedLocationClient() {
        Log.d(TAG, "Building fused location client...");
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fusedLocationClient
                    .requestLocationUpdates(locationRequest, getLocationCallback(), Looper.myLooper());
            googleMap.setMyLocationEnabled(true);

            Log.d(TAG, "API version larger than or equal M");
        } else {
            Log.d(TAG, "API version smaller than M");
            fusedLocationClient
                    .requestLocationUpdates(locationRequest, getLocationCallback(), Looper.myLooper());
            googleMap.setMyLocationEnabled(true);
        }

        Log.d(TAG, "Fused client built");
    }

    /**
     * Method for UI initialization
     */
    private void initUi() {
        Log.d(TAG, "Initing UI...");
        initData();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        snackbar = Snackbar.make(mapFragment.getView(), "No connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("Ok", v -> snackbar.dismiss());

        connectionDisposable.add(Utils.provideInternetConnectionInfo().subscribe(aBoolean -> {
            if (!aBoolean) {
                snackbar.show();
                Log.d(TAG, "No connection to internet");
            } else {
                snackbar.dismiss();
            }
        }));

        Log.d(TAG, "UI inited");
    }

    /**
     * Method for data initialization including ViewModel
     */
    private void initData() {
        Log.d(TAG, "Initing data...");
        ((WunderApplication) getApplication()).getApplicationComponent().inject(this);

        mapsViewModel = ViewModelProviders.of(this, viewModelFactory).get(MapsViewModel.class);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        getLocationPermission();

        Log.d(TAG, "Data inited");
    }

    /**
     * Method for building location request needed for user location
     */
    private void buildLocationRequest() {
        Log.d(TAG, "Building location request...");
        locationRequest = new LocationRequest();
        locationRequest.setInterval(30000);
        locationRequest.setFastestInterval(30000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        Log.d(TAG, "Location request built");
    }

    /**
     * Method that calls fetch from server and displays cars in map
     *
     * @param googleMap
     */
    private void getCarsData(GoogleMap googleMap) {
        Log.d(TAG, "Getting car data...");
        mapsViewModel.fetchCars();
        mapsViewModel.getCars().observe(this, cars -> {
            if (cars != null) {
                Log.d(TAG, "Car fetched");
                showCarsOnMap(cars, googleMap);
            }
        });
    }

    /**
     * Method for creating car markers
     *
     * @param cars
     * @param googleMap
     */
    private void showCarsOnMap(List<Car> cars, GoogleMap googleMap) {
        Log.d(TAG, "Showing cars on map...");
        for (Car car : cars) {
            LatLng carLocation = new LatLng(car.getLat(), car.getLon());
            Marker carMarker = googleMap.addMarker(new MarkerOptions().position(carLocation)
                    .title(getCarTitle(car)));
            carMarker.setTag(car);
            markerList.add(carMarker);
        }
        Log.d(TAG, "Cars shown on map...");
    }

    /**
     * Method for formatting car title depending on its existence
     *
     * @param car
     * @return
     */
    private String getCarTitle(Car car) {
        if (car.getTitle() == null || car.getTitle().trim().isEmpty()) {
            return "No title";
        }
        return car.getTitle();
    }

    /**
     * Method for hiding all car markers form map
     *
     * @param markers
     * @param id
     */
    private void hideAllMarkers(List<Marker> markers, String id) {
        Log.d(TAG, "Hiding markers...");
        for (Marker marker : markers) {
            if (!marker.getId().equals(id)) {
                marker.setVisible(false);
            }
        }
        Log.d(TAG, "Markers hidden");
    }

    /**
     * Method for displaying car markers
     *
     * @param markers
     */
    private void showAllMarkers(List<Marker> markers) {
        Log.d(TAG, "Showing markers...");
        for (Marker marker : markers) {
            marker.setVisible(true);
        }
        Log.d(TAG, "Markers visible");
    }

    private void startDetailsActivity(Integer id) {
        Log.d(TAG, "Starting details activity; carId = " + id);
        Intent detailsActivityIntent = new Intent(this, DetailsActivity.class);
        detailsActivityIntent.putExtra(CAR_ID, id);
        startActivity(detailsActivityIntent);
    }

    private void getLocationPermission() {
        Log.d(TAG, "Checking permissions...");
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            Log.d(TAG, "Permissions granted");
        } else {
            Log.d(TAG, "Permissions not granted; Asking for permissions");
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Mehod for creating LatLong instance of selected location
     *
     * @param location
     * @return
     */
    private LatLng getLatitudeAndLongitude(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        return latLng;
    }

    /**
     * Method that returns LocationCallback instance for tracking user location and displaying it on map
     *
     * @return
     */
    private LocationCallback getLocationCallback() {
        return new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                List<Location> locationList = locationResult.getLocations();
                if (locationList.size() > 0) {
                    Location location = locationList.get(locationList.size() - 1);
                    Log.i(TAG, "Location: " + location.getLatitude() + " " + location.getLongitude());

                    if (!isCurrentLocationInited) {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(getLatitudeAndLongitude(location), 11));
                        isCurrentLocationInited = true;
                    }
                }
            }
        };
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.d(TAG, "Marker clicked");
        hideAllMarkers(markerList, marker.getId());

        if (mapsViewModel.getSelectedCar() == null) {
            mapsViewModel.setSelectedCar((Car) marker.getTag());
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
            googleMap.moveCamera(CameraUpdateFactory.zoomTo(15));
        } else {
            startDetailsActivity(mapsViewModel.getSelectedCar().getCarId());
        }

        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.d(TAG, "Map clicked");
        if (mapsViewModel.getSelectedCar() != null) {
            showAllMarkers(markerList);
            mapsViewModel.setSelectedCar(null);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (connectionDisposable != null) {
            connectionDisposable.clear();
            connectionDisposable = null;
        }
    }
}
