package com.wunder;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.wunder.views.mapview.MapActivity;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "MainActivity created");

        startMapsActivity();
    }

    private void startMapsActivity() {
        Log.d(TAG, "Starting MapsActivity...");
        Intent mapsActivityIntent = new Intent(this, MapActivity.class);
        startActivity(mapsActivityIntent);
        finish();
    }
}
