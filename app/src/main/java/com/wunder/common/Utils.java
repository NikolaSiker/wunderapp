package com.wunder.common;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Class containing static utility methods
 */
public class Utils {

    /**
     * Method for providing network state info - is connected to Internet or not
     *
     * @return
     */

    public static Single<Boolean> provideInternetConnectionInfo() {
        return Single.fromCallable(() -> {
            try {
                int timeoutMs = 1500;
                Socket socket = new Socket();
                InetSocketAddress inetSocketAddress = new InetSocketAddress("8.8.8.8", 53);
                socket.connect(inetSocketAddress, timeoutMs);
                socket.close();

                return true;
            } catch (IOException e) {
                return false;
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
