package com.wunder.repositories;

import com.wunder.services.rentservice.IRentService;

import javax.inject.Inject;

import io.reactivex.Single;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class RentRepository {
    private IRentService rentService;

    /**
     * Class constructor
     *
     * @param rentService
     */
    @Inject
    public RentRepository(IRentService rentService) {
        this.rentService = rentService;
    }

    /**
     * Method for posting new Rent object to REST API
     *
     * @param url
     * @param token
     * @param requestBodyObject
     * @return
     */
    public Single<ResponseBody> createRent(String url, String token, RequestBody requestBodyObject) {
        return rentService.createRent(url, token, requestBodyObject);
    }
}
