package com.wunder.repositories;

import com.wunder.model.Car;
import com.wunder.model.CarDetails;
import com.wunder.services.carservice.ICarService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Response;

/**
 * Class for calling service methods
 */
public class CarRepository {

    private ICarService service;

    /**
     * Class constructor injected with service which methods is being called
     *
     * @param service REST service with network methods
     */
    @Inject
    public CarRepository(ICarService service) {
        this.service = service;
    }

    /**
     * Method for getting list of all cars from REST
     *
     * @return all cars response
     */
    public Single<Response<List<Car>>> getAllCars() {
        return service.getAllCars();
    }

    /**
     * Method for getting one car by id
     *
     * @param id - id of specified car
     * @return response object
     */
    public Single<Response<CarDetails>> getCarByid(String id) {
        return service.getCarById(id);
    }
}
