# Wunder

App that displays car markers on map. On specific marker click (2 times) opens details View with possibility of renting that car.

## Architecture

For this specific app I decided to go with **MVVM** pattern. I used next structure:

- **app**
  - **Common** - package containing common classes as *consts, utils, etc.*
    - **Constants**
    - **Utils**
  - **DI** - Dependency Injection package containing *Components* and *Modules*
    - **Components**
    - **Modules**
  - **Model** - Model classes
  - **Repositories** - Package containing repository classes for *Services*
  - **Services** - Package containing REST api interfaces
  - **Views** - Activities
  - **VM** - ViewModels

## Third-party libraries
 - [**Dagger2**](https://github.com/google/dagger) - For dependency injection
 - [**RxJava**](https://github.com/ReactiveX/RxJava) - For dealing with async requests
- [**RxAndroid**](https://github.com/ReactiveX/RxAndroid) - Same as *RxJava* but with Android specific features
- [**Retrofit**](https://github.com/square/retrofit) - For making network requests
- [**Android Architecture Components**](https://developer.android.com/topic/libraries/architecture) - For ViewModels, LiveData etc.
- [**Butterknife**](https://github.com/JakeWharton/butterknife) - For View Injection 
- [**Glide**](https://github.com/bumptech/glide) - For image loading
- [**Gson**](https://github.com/google/gson) - For JSON serialization/deserialization

## What could be done better?

 - Connection checking - because it is not real time, could be done with combination of Sockets and ConnectivityManager

- Some parts of structure could be less confusing
- More exception checking